import './index.css';
import { ConfigProvider } from 'antd';
import RussianLocale from 'antd/es/locale/ru_RU';
import { configure } from 'mobx';
import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import routes from './config/routes';
import { MobxRouter, startRouter } from 'mobx-router-typescript';
import rootStore from './mobx/root-store';
import { StoreProvider } from './mobx/store-context';
import 'mobx-react-lite/batchingForReactDom';

configure({ enforceActions: 'observed' });
startRouter(routes, rootStore);

ReactDOM.render(
  <ConfigProvider locale={RussianLocale}>
    <StoreProvider value={rootStore}>
      <MobxRouter store={rootStore} />
    </StoreProvider>
  </ConfigProvider>,
  document.getElementById('root'),
);

serviceWorker.unregister();
