import React from 'react';
import { Route } from 'mobx-router-typescript';
import { RootStore } from '../mobx/root-store';
import App from '../components/app/App';
import SignIn from '../components/before-login/auth/sign-in/sign-in';
import Register from '../components/before-login/auth/register/register';

const routes = {
  home: new Route<RootStore>({
    path: '/',
    component: <App />,
  }),
  signIn: new Route<RootStore>({
    path: '/sign_in',
    component: <SignIn />,
  }),
  register: new Route<RootStore>({
    path: '/register',
    component: <Register />,
  }),
};
export default routes;
