import React from 'react';
import StartPage from '../before-login/info-pages/start-page/start-page';
import { observer } from 'mobx-react';

const App: React.FunctionComponent = observer(() => <StartPage />);

export default App;
