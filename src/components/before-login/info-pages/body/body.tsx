import './body.css';
import React from 'react';
import { Button, Typography } from 'antd';

const Body: React.FunctionComponent = () => (
  <div className="body-container">
    <Typography.Title className="body-title">
      {'Место, где каждый может'}
      <br />
      {'поделиться своими знаниями'}
    </Typography.Title>
    <Typography.Text className="body-description">
      {'Добро пожаловать в онлайн платформу для работы с онтологиями'}
    </Typography.Text>
    <Button className="register-button" type="primary" shape="round" size="large">
      {'Зарегистрироваться'}
    </Button>
  </div>
);
export default Body;
