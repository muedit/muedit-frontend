import './header.css';
import React, { useContext } from 'react';
import { Button, Typography } from 'antd';
import { ArrowRightOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react';
import { Link } from 'mobx-router-typescript';
import routes from '../../../../config/routes';
import { StoreContext } from '../../../../mobx/store-context';

const Header: React.FunctionComponent = observer(() => {
  const store = useContext(StoreContext);
  return (
    <div className="header-container">
      <div className="menu">
        <Link view={routes.home} store={store}>
          <Typography.Text className="logo">{'MuEdit'}</Typography.Text>
        </Link>
        <Typography.Text className="menu-item">{'Плагины'}</Typography.Text>
        <Typography.Text className="menu-item">{'Контакты'}</Typography.Text>
      </div>
      <div className="login-buttons">
        <Link view={routes.signIn} store={store}>
          <Typography.Text className="menu-item">{'Войти'}</Typography.Text>
        </Link>
        <Link view={routes.register} store={store}>
          <Button
            className="login-button"
            type="primary"
            shape="round"
            size="large"
            icon={<ArrowRightOutlined />}
          >
            {'Зарегистрироваться'}
          </Button>
        </Link>
      </div>
    </div>
  );
});

export default Header;
