import Header from '../header/header';
import React from 'react';
import Body from '../body/body';
import { observer } from 'mobx-react';

const StartPage: React.FunctionComponent = observer(() => (
  <div>
    <Header />
    <Body />
  </div>
));
export default StartPage;
