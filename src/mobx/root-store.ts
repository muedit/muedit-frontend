import { RouterStore } from 'mobx-router-typescript';

export class RootStore {
  public router: RouterStore = new RouterStore();
}

const rootStore = new RootStore();
export default rootStore;
